import dotenv from 'dotenv';
dotenv.config();

import express from 'express';
import Mongo from 'mongoose';    
import bodyParser from 'body-parser';

import router from './router/router.js';

const app = express();

app.use(bodyParser.json());
app.use('/api', router);

Mongo.connect(
    process.env.MONGO_ATLAS_URL,
    () => console.log(`[+] Connected To Database.`)
);

app.listen(process.env.PORT, () => console.log(`[+] Server Started On PORT ${process.env.PORT}..`));