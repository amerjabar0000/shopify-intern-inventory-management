import { Router } from "express";
import Hateoas from "../helper/hateoas.js";
import item from "../models/item.js";
import category from "../models/category.js";

const router = Router();
const hateoasEngine = new Hateoas('127.0.0.1:4444');

router.get('/', (req, res) => res.status(200).send({ message: 'You reached the base endpoint' }));

// All routes related to items model
router.get('/items', async (req, res) => {
    try {
        const items = await item.find({}).populate('category');
        res.status(200).send(hateoasEngine.toLink(items, 'items'));
    } catch (e) {
        res.status(404).send({ message: 'Not found' });
    }
})

router.get('/items/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const itemElement = await item.findById(id).populate('category');
        res.status(200).send(hateoasEngine.toLink(itemElement, 'items'));
    } catch (e) {
        res.status(404).send({ message: 'Not found' });
    }
})

router.post('/items', async (req, res) => {
    const itemBody = req.body?.itemBody;
    try {
        const itemElement = await item.create(itemBody);
        res.status(200).send(hateoasEngine.toLink(itemElement));
    } catch (e) {
        res.status(409).send({ message: 'Not able to create' });
    }
})

router.put('/items/:id', async (req, res) => {
    const { id } = req.params;
    const itemBody = req.body?.itemBody;
    try {
        await item.findByIdAndUpdate(id, itemBody).populate('category');
        const itemElement = await item.findById(id)
        res.status(200).send(hateoasEngine.toLink(itemElement));
    } catch (e) {
        res.status(409).send({ message: 'Not able to create' });
    }
})

router.delete('/items/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const itemElement = await item.findById(id).populate('category');
        await item.findByIdAndDelete(id);
        res.status(200).send(hateoasEngine.toLink(itemElement));
    } catch (e) {
        res.status(409).send({ message: 'Not able to delete' });
    }
})

// All routes related to category model
router.get('/categories', async (req, res) => {
    try {
        const categories = await category.find();
        res.status(200).send(hateoasEngine.toLink(categories, 'categories'));
    } catch (e) {
        res.status(404).send({ message: 'Not found' });
    }
})

router.get('/categories/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const categoryElement = await category.findById(id).populate('items');
        res.status(200).send(hateoasEngine.toLink(categoryElement, 'categories'));
    } catch (e) {
        res.status(404).send({ message: 'Not found' });
    }
})

router.post('/categories', async (req, res) => {
    const categoryBody = req.body?.categoryBody;
    try {
        const categoryElement = await category.create(categoryBody);
        res.status(200).send(hateoasEngine.toLink(categoryElement, 'categories'));
    } catch (e) {
        res.status(404).send({ message: 'Not able to create' });
    }
})

router.put('/categories/:id', async (req, res) => {
    const { id } = req.params;
    const categoryBody = req.body?.categoryBody;
    try {
        await category.findByIdAndUpdate(id, categoryBody);
        const categoryElement = await category.findById(id);
        res.status(200).send(hateoasEngine.toLink(categoryElement, 'categories'));
    } catch (e) {
        res.status(404).send({ message: 'Not found' });
    }
})

router.delete('/categories/:id', async (req, res) => {
    const { id } = req.params;
    try {
        const categoryElement = await category.findById(id);
        await category.findByIdAndDelete(id);
        res.status(200).send(hateoasEngine.toLink(categoryElement, 'categories'));
    } catch (e) {
        res.status(409).send({ message: 'Not found' });
    }
})

export default router;