### Shopify Challenge - Inventory Application

###### Note: This application is an API that is developed in Node.js and is connected to a Mongo database.

Running on: https://shopify-inventory-system.azurewebsites.net/

The application is containerized with docker and hosted on azure platform.

#### How to use the API
###### *.Open a browser (or an API tester application like postman) and go to the url/api where you reach the base endpoint.
###### *.Do an HTTP call to the API and get a response back.

#### Structure of the API
###### - The API responds to requests that are directed towards two models: items and categories
###### - For each model, HTTP methods (GET, POST, PUT and DELETE) are applicable.

#### Example:
###### 1.Do an HTTP call on https://shopify-inventory-system.azurewebsites.net/api/items with the following properties:
###### METHOD: POST, BODY: { name: 'Jacket', price: 89.99, quantity: 2, company: 'Shopify', category: '61e85a50fd6750f3a87fe35b' }
###### The response should be the object created with the data.


##### Further notes: using put method, relations can be made between items and categories. Each category can contain many items. And each item can have one parent category.



