class Hateoas {

    static host;

    constructor(host) {
        Hateoas.host = host;
    }

    objectToLink(data, modelName) {
        const response = {
            ...data._doc,
            _links: {
                self: {
                  href: `https://${Hateoas.host}/${modelName}/${data._id}`
                }
            }
        }

        response._links[modelName] = {
            href: `https://${Hateoas.host}/${modelName}`
        }

        return response;
    }

    arrayToLink(data, modelName) {
        const response = {
            _embedded: {},
            _links: {
                self: {
                  href: `https://${Hateoas.host}/${modelName}`
                },
                root: {
                  href: `https://${Hateoas.host}`
                }
            }
        }

        if ( data )
            response._embedded[modelName] = data.map(item => this.objectToLink(item, modelName))

        return response;
    }

    toLink(data, modelName) {
        if ( typeof(data) === "object" && !Array.isArray(data) && data !== null ) {
            return this.objectToLink(data, modelName);
        } else if ( Array.isArray(data) ) {
            return this.arrayToLink(data, modelName);
        }
    }

}


export default Hateoas;