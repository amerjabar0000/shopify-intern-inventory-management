FROM node:16.13.0

COPY package*.json ./

COPY . ./

# Install deps
RUN npm i
RUN npm i nodemon -g

ENV PORT 80

EXPOSE 80

CMD ["npm", "start"]