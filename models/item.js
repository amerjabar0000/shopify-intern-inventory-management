import mongoose from 'mongoose';

const { Schema, SchemaTypes, model } = mongoose;

const itemSchema = new Schema({
    name: SchemaTypes.String,
    dateAdded: SchemaTypes.Date,
    price: SchemaTypes.Number,
    quantity: SchemaTypes.Number,
    company: SchemaTypes.String,
    category: {
        type: SchemaTypes.ObjectId,
        ref: "Category"
    }
})

export default model('Item', itemSchema);