import mongoose from 'mongoose';

const { Schema, SchemaTypes, model } = mongoose;

const categorySchema = new Schema({
    name: SchemaTypes.String,
    description: SchemaTypes.String,
    entities: {
        type: SchemaTypes.Number,
        default: 0
    },
    items: [{
        type: SchemaTypes.ObjectId,
        ref: "Item"
    }]
})

export default model('Category', categorySchema);